package spost.item;

import android.widget.ImageView;

import java.util.HashMap;

public class SidemenuItem {

	private int mSequence;

	private String mController;
	private String mPosition;
	private String mRequire;
	private String mName;
	private String mIconName;
	private String mUrl;
	private String mTel;
	private String mEMail;
	private String mViewType;

	private ImageView mIcon;
	
	private HashMap<String, String> mHashMpSocialItem;
	
	/**
	 * コンストラクタ
	 */
	public SidemenuItem() {
		initialize();
	}

	/**
	 * セッター
	 * @param
	 */
	public void setSequence(int sequence) {
		this.mSequence = sequence;
	}

	public void setController(String controller) {
		this.mController = controller;
	}

	public void setPosition(String position) {
		this.mPosition = position;
	}
	
	public void setRequire(String require) {
		this.mRequire = require;
	}
	
	public void setName(String name) {
		this.mName = name;
	}

	public void setIconName(String iconName) {
		this.mIconName = iconName;
	}
	
	public void setUrl(String url) {
		this.mUrl = url;
	}

	public void setTel(String tel) {
		this.mTel = tel;
	}

	public void setEMail(String eMail) {
		this.mEMail =eMail;
	}

	public void setViewType(String viewType) {
		this.mViewType = viewType;
	}

	public void setIcon(ImageView icon) {
		this.mIcon = icon;
	}

	public void sethashMpSocialItem(HashMap<String, String> SocialItem) {
		this.mHashMpSocialItem = SocialItem;
	}

	
	/**
	 * ゲッター
	 * @return
	 */

	public int getSequence() {
		return this.mSequence;
	}
	
	public String getController() {
		return this.mController;
	}
	
	public String getPosition() {
		return this.mPosition;
	}
	
	public String getRequire() {
		return this.mRequire;
	}
	
	public String getName() {
		return this.mName;
	}

	public String getIconName() {
		return this.mIconName;
	}
	
	public String getUrl() {
		return this.mUrl;
	}
	
	public String getTel() {
		return this.mTel;
	}
	
	public String getEMail() {
		return this.mEMail;
	}
	
	public String getViewType() {
		return this.mViewType;
	}
	
	public ImageView getIcon() {
		return this.mIcon;
	}
	
	public HashMap<String, String> getHashMpSocialItem() {
		return this.mHashMpSocialItem;
	}

	
	/**
	 * メンテナンス情報初期化
	 */
	public void initialize(){
		mSequence = 0;
		
		mController = "";
		mPosition   = "";
		mRequire    = "";
		mName       = "";
		mIconName   = "";
		mUrl        = "";
		mTel        = "";
		mEMail      = "";
		mViewType   = "";

		mIcon = null;
		
		mHashMpSocialItem = null;
	}
	
}