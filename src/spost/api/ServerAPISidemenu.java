package spost.api;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import spost.asynctask.AsyncTaskImageLoader;
import spost.item.SidemenuItem;
import jp.co.hiropro.spost.test_team.Globals;

public class ServerAPISidemenu extends ServerAPIAbstract {

	private static Globals sGlobals;
	private static Context sContext;

	/**
	 * コンストラクタ
	 * @param context
	 */
	public ServerAPISidemenu(Context context) {
		super(context);
	}

	/**
	 * ServerAPIMainteインスタンス取得
	 * @param context
	 * @param globals
	 * @return
	 */
	public static ServerAPISidemenu getInstance(Context context, Globals globals) {
		sGlobals = globals;
		sContext = context;
		ServerAPISidemenu serverAPI = new ServerAPISidemenu(context);
		return serverAPI;
	}

	@Override
	String getPrefName() {
		return sGlobals.getPrefKeyName();
	}

	@Override
	String getPrefKeyData() {
		return sGlobals.getPrefKeySidemenu();
	}

	@Override
	String getApiURLData() {
		return sGlobals.getApiSidemenu();
	}

	@Override
	HashMap<String, String> getApiURLParamsData() {
		HashMap<String, String> params = new HashMap<String, String>();
		return params;
	}


	/**
	 * サイドメニューリスト取得
	 *
	 * ＡＰＩサーバーからＪＳＯＮデータを取得、
	 * サイドメニュー項目のメニュー毎の配列を返却
	 * @return ArrayList<SidemenuItem> : メニュー項目毎の配列
	 */
	public ArrayList<SidemenuItem> getSideMenuList() {
		
		ArrayList<SidemenuItem> mSideMenuList = null;
		
		// ＪＳＯＮデータ取得
		JSONObject mJsObj = getJsonObjWhole();
		
		if (mJsObj != null) {
			
			// メニューリスト(ＪＳＯＮ配列)を取得
			JSONArray mJsAryMenuList = mJsObj.optJSONArray("list_menu");
			
			if (mJsAryMenuList != null) {
				
				mSideMenuList = new ArrayList<SidemenuItem>();
				
				for (int i=0; i < mJsAryMenuList.length(); i++) {
					
					SidemenuItem mSideMenuItem = new SidemenuItem();
					// メニューリストを一列取得
					JSONObject mJsObjMenu = mJsAryMenuList.optJSONObject(i);
					
					if (mJsObjMenu.isNull("controller") != true) {
						mSideMenuItem.setController(mJsObjMenu.optString("controller"));			
					}
					if (mJsObjMenu.isNull("position") != true) {
						mSideMenuItem.setPosition(mJsObjMenu.optString("position"));			
					}
					if (mJsObjMenu.isNull("require") != true) {
						mSideMenuItem.setRequire(mJsObjMenu.optString("require"));			
					}
					if (mJsObjMenu.isNull("name") != true) {
						mSideMenuItem.setName(mJsObjMenu.optString("name"));			
					}
					if (mJsObjMenu.isNull("icon") != true) {
						mSideMenuItem.setIconName(mJsObjMenu.optString("icon"));
						// メニューアイコン取得
						String mIconUrl = sGlobals.getUrlImgGen() + mJsObjMenu.optString("icon");
						ImageView mIconView = this.getIconView(mIconUrl);
						mSideMenuItem.setIcon(mIconView);
					}
					if (mJsObjMenu.isNull("url") != true) {
						mSideMenuItem.setUrl(mJsObjMenu.optString("url"));			
					}
					if (mJsObjMenu.isNull("tel") != true) {
						mSideMenuItem.setTel(mJsObjMenu.optString("tel"));			
					}
					if (mJsObjMenu.isNull("email") != true) {
						mSideMenuItem.setEMail(mJsObjMenu.optString("email"));			
					}
					if (mJsObjMenu.isNull("view_type") != true) {
						mSideMenuItem.setViewType(mJsObjMenu.optString("view_type"));			
					}
					
					mSideMenuList.add(mSideMenuItem);
					
				}	
			}
		}
		
		if (mSideMenuList != null) {
			// 表示順序ごとにシーケンス番号を設定
			// ソート等の処理はここで行う(暫定：取得順序＝表示順序)
			for (int i = 0; i < mSideMenuList.size(); i++) {
				mSideMenuList.get(i).setSequence(i + 1);
			}
		}
		
		return mSideMenuList;
	}	

	/**
	 * ＳＮＳ項目取得
	 * @return HashMap<String, String> : ＳＮＳ名とそのＵＲＬの連想配列(key:ＳＮＳ名)
	 */	
	public HashMap<String, String> getSocialItem() {
		
		HashMap<String, String> mHsMpSocialItem = new HashMap<String, String>();
		
		// ＪＳＯＮデータ取得
		JSONObject mJsObj = getJsonObjWhole();
		
		if (mJsObj != null) {
			// ＳＮＳ項目を取得し、各ＳＮＳ毎に配列に格納
			JSONObject mJsObjSocial = mJsObj.optJSONObject("list_social");

			if (mJsObjSocial != null) {
				
				String mSocialList[] = sGlobals.getSocialList();
				
				for (int i=0; i < mSocialList.length; i++) {
					
					if (mJsObjSocial.isNull(mSocialList[i]) != true) {
						mHsMpSocialItem.put(mSocialList[i], mJsObjSocial.optString(mSocialList[i]));
					}
				}
			}
		}
		
		return mHsMpSocialItem;
		
	}
	
	private JSONObject getJsonObjWhole() {
		
		JSONObject mJsonObj = null;
		
		try {
			String mJsonStr = (String)this.getCurrentData();
			mJsonObj = new JSONObject(mJsonStr);			
		} catch (JSONException e) {
			e.printStackTrace();
			Log.v("YOSHITAKA", "JSON DATA is Not Getting. : " + e.getMessage());
		}
		
		return mJsonObj;
	}
	
	private ImageView getIconView(String url) {
		
		AsyncTaskImageLoader task = new AsyncTaskImageLoader(sContext);
		task.execute(url);
		ImageView mImageView = new ImageView(sContext);
		try {
			mImageView.setImageBitmap(task.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
			mImageView = null;
		} catch (ExecutionException e) {
			e.printStackTrace();
			mImageView = null;
		}
        
        return mImageView;
        
	}
	
}