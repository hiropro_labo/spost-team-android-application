package spost.asynctask;

import spost.api.ServerAPIMainte;
import spost.api.ServerAPISidemenu;
import jp.co.hiropro.spost.test_team.Globals;

import android.content.Context;
import android.os.AsyncTask;

import com.duarise.bundlehelper.PreferenceHelper;

public class AsyncTaskApi extends AsyncTask<Void, Void, Void> {

	private int flg;

	private PreferenceHelper mPreferenceHelper;

	private Context mContext;
	private Globals mGlobals;

	/**
	 * コンストラクタ
	 * @param activity (※引数は基本的に実装任せであって必須ではない）
	 */
	public AsyncTaskApi(Context context, Globals globals) {
		mContext = context;
		mGlobals = globals;
	}

	/**
	 * バックグラウンド処理
	 * ※UI関連処理はonPostExecuteメソッドへ実装
	 */
	@Override
	protected Void doInBackground(Void... params) {
		ServerAPIMainte.getInstance(mContext, mGlobals).update();
		ServerAPISidemenu.getInstance(mContext, mGlobals).update();

//追加↓
		//APIロードフラグ保存
		flg = 1;
		this.mPreferenceHelper = this.getPreferenceHelper();
		this.mPreferenceHelper.put(this.mGlobals.getPrefKeyLoaded(), flg);
//追加↑
		
		return null;
	}

	/**
	 * UI関連処理はこのメソッドへ実装(※スレッド処理完了後にコールされる)
	 * @param params
	 * @return
	 */
	@Override
	protected void onPostExecute(Void result) {
		// ※メインスレッドから呼ばれるのでUI関連操作が可能

	}
	
	/**
	 * PreferenceHelperインスタンス取得: データ用
	 * @return
	 */
	//プリファレンスの値保存までの内容をひとまとめで処理
	private PreferenceHelper getPreferenceHelper() {
		if (mPreferenceHelper == null) {
			mPreferenceHelper = PreferenceHelper.getInstance(mContext, this.mGlobals.getPrefKeyName());
		}
		return mPreferenceHelper;
	}
	
	//保存したAPIロードフラグの値を取得
	public int getSharedPreferences_Flg(){
		this.mPreferenceHelper = this.getPreferenceHelper();
		int flg = (Integer) this.mPreferenceHelper.get(this.mGlobals.getPrefKeyLoaded(), int.class);
		return flg;
	}
}
