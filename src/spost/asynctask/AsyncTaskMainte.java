package spost.asynctask;

import jp.co.hiropro.spost.test_team.Globals;
import spost.api.ServerAPIMainte;
import spost.api.ServerAPISidemenu;

import android.content.Context;
import android.os.AsyncTask;

//import com.duarise.bundlehelper.PreferenceHelper;

public class AsyncTaskMainte extends AsyncTask<Void, Void, Boolean> {

	private Context mContext;
	private Globals mGlobals;

//	private PreferenceHelper mPreferenceHelper;


	/**
	 * コンストラクタ
	 * @param Context
	 * @param Globals
	 */
	public AsyncTaskMainte(Context context, Globals globals) {
		mContext = context;
		mGlobals = globals;
	}

	/**
	 * バックグラウンド処理
	 * ※UI関連処理はonPostExecuteメソッドへ実装
	 */
	@Override
	protected Boolean doInBackground(Void... params) {
		
		ServerAPIMainte.getInstance(mContext, mGlobals).update();
		ServerAPISidemenu.getInstance(mContext, mGlobals).update();
		
		// 現在、更新処理が完了したら無条件で「true」を返却
		return true;
	}

	/**
	 * UI関連処理はこのメソッドへ実装(※スレッド処理完了後にコールされる)
	 * @param params
	 * @return
	 */
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
	}
	
	/**
	 * PreferenceHelperインスタンス取得: データ用
	 * @return
	 */
//	//プリファレンスの値保存までの内容をひとまとめで処理
//	private PreferenceHelper getPreferenceHelper() {
//		if (mPreferenceHelper == null) {
//			mPreferenceHelper = PreferenceHelper.getInstance(mContext, this.mGlobals.getPrefKeyName());
//		}
//		return mPreferenceHelper;
//	}
//	
//	//保存したAPIロードフラグの値を取得
//	public int getSharedPreferences_Flg(){
//		this.mPreferenceHelper = this.getPreferenceHelper();
//		int loadedFlg = (Integer) this.mPreferenceHelper.get(this.mGlobals.getPrefKeyLoaded(), int.class);
//		return loadedFlg;
//	}
}
