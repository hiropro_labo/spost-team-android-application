package spost.item;

public class MainteItem {

	private String mMainte;
	
	/**
	 * コンストラクタ
	 */
	public MainteItem() {
		initialize();
	}
	
	/**
	 * セッター
	 * @param
	 */
	public void setMaintenance(String mainte) {
		this.mMainte = mainte;
	}


	/**
	 * ゲッター
	 * @return
	 */
	public String getMainte() {
		return this.mMainte;
	}
	
	
	/**
	 * メンテナンス情報初期化
	 */
	public void initialize(){
		mMainte     = "";
	}
	
}