package spost.api;

import android.content.Context;
import android.util.Log;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import jp.co.hiropro.spost.test_team.Globals;
import spost.item.MainteItem;

/**
 * @author yoshitaka
 */
public class ServerAPIMainte extends ServerAPIAbstract {

	private static Globals sGlobals;

	/**
	 * コンストラクタ
	 * @param context
	 */
	public ServerAPIMainte(Context context) {
		super(context);
	}

	/**
	 * ServerAPIMainteインスタンス取得
	 * @param context
	 * @param globals
	 * @return
	 */
	public static ServerAPIMainte getInstance(Context context, Globals globals) {
		sGlobals = globals;
		ServerAPIMainte serverAPI = new ServerAPIMainte(context);
		return serverAPI;
	}

	@Override
	String getPrefName() {
		return sGlobals.getPrefKeyName();
	}

	@Override
	String getPrefKeyData() {
		return sGlobals.getPrefKeyMainte();
	}

	@Override
	String getApiURLData() {
		return sGlobals.getApiMainte();
	}

	@Override
	HashMap<String, String> getApiURLParamsData() {
		HashMap<String, String> params = new HashMap<String, String>();
		return params;
	}
	
	/**
	 * メンテナンス情報の取得
	 * @return
	 */
	public MainteItem getMainteItem() {
		
		// ＪＳＯＮデータ取得
		JSONObject mJsObjMainte = null;
		try {
			mJsObjMainte = new JSONObject((String) this.getCurrentData());
		} catch (JSONException e) {
			Log.v("YOSHITAKA", "MAINTE ITEM : getMainteItem : error : " + e.getMessage());
		}
		
		MainteItem mMainteItem = new MainteItem();
		
		if (mJsObjMainte != null) {
			
			if (mJsObjMainte.isNull("maintenance") != true) {
				mMainteItem.setMaintenance(mJsObjMainte.optString("maintenance"));
			}
		}
		
		return mMainteItem;
	}
	
}